//
//  RatingControl.swift
//  FoodTracker
//
//  Created by Jakkrits on 10/19/2559 BE.
//  Copyright © 2559 Jakk. All rights reserved.
//

import UIKit

class RatingControl: UIView {
    
    var rating = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    var ratingButtons = [UIButton]()
    let spacing = 8
    let startCount = 5
    
    override var intrinsicContentSize: CGSize {
        
        return CGSize(width: 240, height: 44)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        for _ in 0..<startCount {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "emptyStar"), for: .normal)
            button.setImage(#imageLiteral(resourceName: "filledStar"), for: .selected)
            button.setImage(#imageLiteral(resourceName: "filledStar"), for: [.highlighted, .selected])
            button.adjustsImageWhenHighlighted = false
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchDown)
            ratingButtons += [button]
            addSubview(button)
        }
    }
    
    override func layoutSubviews() {
        let buttonSize = Int(frame.size.height)
        print(buttonSize)
        var buttonFrame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        let buttons = ratingButtons.enumerated()
        
        for (index, button) in buttons {
            buttonFrame.origin.x = CGFloat(index * (buttonSize + spacing))
            button.frame = buttonFrame
        }
        updateButtonSelectionStates()
    }
    
    func ratingButtonTapped(button: UIButton) {
        print("button pressed")
        rating = ratingButtons.index(of: button)! + 1
        updateButtonSelectionStates()
    }
    
    func updateButtonSelectionStates() {
        let buttons = ratingButtons.enumerated()
        for (index, button) in buttons {
            button.isSelected = index < rating
        }
    }
}
